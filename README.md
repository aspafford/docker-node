# README #

This app will run a "hello world" node application on a Docker image. 

Below are instructions for running multiple Docker images on a single aws EC2 instance.

### Setup EC2 Instance on AWS ###

* Create EC2 instance (latest ubuntu, t2.medium, 40G Disk Space)

### Install Docker ###

* ssh into EC2 instance as ubuntu user, then...
* `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`
* `sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`
* `sudo apt-get update`
* `sudo apt-get install -y docker-ce`
* `sudo usermod -aG docker ${USER}` 

### Install Nginx ###

* `sudo apt-get install nginx`

### Deploy this Test Node App ###

* `git clone https://aspafford@bitbucket.org/aspafford/docker-node.git docker-node-test`
* `docker build -t aspafford/docker-node-test .`
* `docker run -p 9000:8080 -d aspafford/docker-node-test`

### Configure Nginx as Reverse Proxy ###

add this to nginx.conf file:

```
server {
  listen 80;
  server_name docker-test.mysite.com;
  location / {
    proxy_pass http://127.0.0.1:9000;
  }
}
```

### Configure Subdomain in Route 53 ###

* add subdomain redirecting to EC2 ip address


... 'Hello World!' should now appear at docker-test.mysite.com

(note, if you navigate directly to EC2 ip in the browser you should see the nginx default page)

## Automate App Deployment ##

The following steps show how to trigger app deployment using a git commit command

### Configure Git Post-Receive Hook ###

On remote server...

create folder that will hold a copy of your project code

`mkdir ~/my-project`

create a *bare* git repository at same directory level

`git init --bare ~/my-project.git`

add a new file: `~/my-project.git/hooks/post-receive`

this file will 1) copy code to ~/my-project folder 2) launch docker build commands:

```
#!/bin/bash
TARGET="/home/ubuntu/my-project"
GIT_DIR="/home/ubuntu/my-project.git"
BRANCH="master"

DOCKER_NAME="my-project"
DOCKER_PORT=8083

while read oldrev newrev ref
do
        # only checking out the master (or whatever branch you would like to deploy)
        if [[ $ref = refs/heads/$BRANCH ]];
        then
                echo "Ref $ref received. Deploying ${BRANCH} branch to production..."
                git --work-tree=$TARGET --git-dir=$GIT_DIR checkout -f

                # docker stuff
                cd $TARGET
                docker build -t $DOCKER_NAME .
                docker rm $(docker stop $(docker ps -a -q --filter "name=$DOCKER_NAME" --format="{{.ID}}"))
                docker run --name $DOCKER_NAME -p $DOCKER_PORT:8080 -d $DOCKER_NAME:latest

        else
                echo "Ref $ref received. Doing nothing: only the ${BRANCH} branch may be deployed on this server."
        fi
done
```

make this file executable:

`chmod +x ~/my-project.git/hooks/post-recieve`

### Setup Local Codebase ###

now on local machine set up codebase to point to remote bare git repo...

first we'll setup a ssh config file so we can use aws pem file when pushing to remote server

```
Host docker-aws
  Hostname 34.212.27.228
  User ubuntu
  IdentityFile ~/production.pem
```

add a 'production' remote to local git repo:

`git remote add production docker-aws:my-project.git`

now when pushing updates to production you should see build commands running from post-receive script

`git push production master` ... [output from script] ...

and docker image should be running on port assigned in script


### Configure sub-domain ###

On AWS, go to Route 53, and create a new subdomain pointing to ip address of our docker host server:

** my-project.4dapt.com ** -> ** 34.212.27.228 **

And, finally, on remote server, edit nginx.conf linking the subdmoain we just created to the port number where our app is running on docker

`vim /etc/nginx/nginx.conf`

```
# My Cool App
server {
  listen 80;
  server_name my-project.4dapt.com;
  location / {
    proxy_pass http://127.0.0.1:8083;
  }
}
```

restart nginx:

`sudo service nginx restart`


app should now be running at my-project.4dapt.com (note it might take a minute for nginx to complete restart)
